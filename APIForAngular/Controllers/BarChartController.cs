﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIForAngular.Models;

namespace APIForAngular.Controllers
{
    public class BarChartController : ApiController
    {
        BarChartData[] barcharts = new BarChartData[] 
        { 
            new BarChartData { name="America", value=100000 }, 
            new BarChartData { name="India", value=5000000 }
        };

        public IEnumerable<BarChartData> GetAllBarChartData()
        {
            return barcharts;
        }

    }
}
